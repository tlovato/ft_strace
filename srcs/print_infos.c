#include "../incs/ft_strace.h"
#include "../incs/syscall_table.h"

static void			init_args(struct user_regs_struct regs, unsigned long long int *args)
{
	args[0] = regs.rdi;
	args[1] = regs.rsi;
	args[2] = regs.rdx;
	args[3] = regs.rcx;
	args[4] = regs.r8;
	args[5] = regs.r9;
}

void				print_infos(struct user_regs_struct regs)
{
	int			i = 0;
	unsigned long long int	args[6];

	init_args(regs, args);
	printf("%s(", table[regs.orig_rax].name);
	while (table[regs.orig_rax].t_args[i])
	{
		if (!args[i] && table[regs.orig_rax].t_args[i] == "%p")
			printf("NULL");
		else
			printf(table[regs.orig_rax].t_args[i], args[i]);
		if (table[regs.orig_rax].t_args[i + 1])
			printf(", ");
		i++;
	}
	printf(")");
}

void				print_return(struct user_regs_struct regs)
{
	printf(" = ");
	if (table[regs.orig_rax].name == "exit_group")
		printf("?");
	else
		printf(table[regs.orig_rax].ret, regs.rax);
	printf("\n");	
}

static void			init_args32(struct user_regs_struct_32 regs, unsigned long long int *args)
{
	args[0] = regs.edi;
	args[1] = regs.esi;
	args[2] = regs.edx;
	args[3] = regs.ecx;
}

void				print_infos32(struct user_regs_struct_32 regs)
{
	int			i = 0;
	unsigned long long int	args[6];

	init_args32(regs, args);
	printf("%s(", table[regs.orig_eax].name);
	while (table[regs.orig_eax].t_args[i])
	{
		if (!args[i] && table[regs.orig_eax].t_args[i] == "%p")
			printf("NULL");
		else
			printf(table[regs.orig_eax].t_args[i], args[i]);
		if (table[regs.orig_eax].t_args[i + 1])
			printf(", ");
		i++;
	}
	printf(")");
}

void				print_return32(struct user_regs_struct_32 regs)
{
	printf(" = ");
	if (table[regs.orig_eax].name == "exit_group")
		printf("?");
	else
		printf(table[regs.orig_eax].ret, regs.eax);
	printf("\n");	
}
