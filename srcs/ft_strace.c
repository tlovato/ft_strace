/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strace.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/28 15:08:35 by tlovato           #+#    #+#             */
/*   Updated: 2019/02/28 15:08:37 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_strace.h"

int				error_handling(char *message)
{
	printf("ft_strace : %s\n", message);
	exit(EXIT_FAILURE);
}

int				main(int ac, char **av, char **env)
{
	char		**path;
	char		*command;

	if (ac <= 1)
		return (error_handling("must have PROG [ARGS]."));
	if (!(path = get_path(env)))
		return(error_handling("path missing."));
	if (!(command = find_command(av[1], path)))
		return (error_handling("invalid executable."));
	if (!trace(command, &av[1], env))
		return (error_handling("error."));
	return (1);
}
