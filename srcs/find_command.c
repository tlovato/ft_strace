/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_command.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/28 16:29:22 by tlovato           #+#    #+#             */
/*   Updated: 2019/02/28 16:29:23 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_strace.h"

char				*find_command(char *cmd, char **path)
{
	int				i = 0;
	DIR				*dir;
	struct dirent	*rdir;

	if (!access(cmd, X_OK))
		return (cmd);
	while (path[i])
	{
		if ((dir = opendir(path[i])))
		{
			while ((rdir = readdir(dir)))
				if (!strcmp(rdir->d_name, cmd))
				{
					path[i] = ft_strjoinfree(path[i], "/", 1);
					path[i] = ft_strjoinfree(path[i], cmd, 1);
					return (path[i]);
				}
			closedir(dir);
		}
		i++;
	}
	return (NULL);
}
