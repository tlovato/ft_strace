/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   trace.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/28 15:17:23 by tlovato           #+#    #+#             */
/*   Updated: 2019/02/28 15:17:24 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_strace.h"

static void				init_sig(sigset_t *empty, sigset_t *blocked)
{
	sigemptyset(empty);
	sigaddset(blocked, SIGHUP);
	sigaddset(blocked, SIGINT);
	sigaddset(blocked, SIGQUIT);
	sigaddset(blocked, SIGPIPE);
	sigaddset(blocked, SIGTERM);
}

static int				check_sig(int status)
{
	
	if (WIFEXITED(status))
	{
		printf("+++ exited with %d +++\n", WEXITSTATUS(status));
		return (1);
	}
	if (WIFSIGNALED(status))
	{
		printf("+++ SIGNAL %d +++\n", WTERMSIG(status));
		return (1);
	}
	if (WIFSTOPPED(status) && WSTOPSIG(status) == 11)
	{
		printf("+++ killed by SIGSEGV +++\n");
		return (1);
	}
	return (0);
}

static int				trace_loop(pid_t pid)
{
	struct user_regs_struct		regs;
	struct user_regs_struct_32	regs32;
	int				status;
	sigset_t			empty;
	sigset_t			blocked;

	init_sig(&empty, &blocked);
	while (1)
	{
		/* enter next syscall */
		ptrace(PTRACE_SYSCALL, pid, 0, 0);
		sigprocmask(SIG_SETMASK, &empty, NULL);
		waitpid(pid, &status, 0);
		sigprocmask(SIG_BLOCK, &blocked, NULL);
		if (check_sig(status))
			break;
		/* get arguments */
		ptrace(PTRACE_GETREGS, pid, 0, &regs);
		if (regs.orig_rax > 314)
		{
			ptrace(PTRACE_GETREGS, pid, 0, &regs32);
			print_infos32(regs32);
		}
		else
			print_infos(regs);
		/* run syscall and stop on exit */
		ptrace(PTRACE_SINGLESTEP, pid, 0, 0);
		sigprocmask(SIG_SETMASK, &empty, NULL);
		waitpid(pid, &status, 0);
		sigprocmask(SIG_BLOCK, &blocked, NULL);	
		/* get return value */
		ptrace(PTRACE_GETREGS, pid, 0, &regs);
		if (regs.orig_rax > 314)
		{
			ptrace(PTRACE_GETREGS, pid, 0, &regs32);
			print_return32(regs32);
		}
		else
			print_return(regs);
		if (check_sig(status))
			break;
	}
}

int					trace(char *path, char **args, char **env)
{
	pid_t 				pid = fork();
	sigset_t			empty;
	sigset_t			blocked;

	init_sig(&empty, &blocked);
	if (!pid)
	{
		/* besoin de cette ligne pour que le pere et le fils 's\'attendent' */
		kill(getpid(), SIGSTOP);
		execve(path, args, env);
		error_handling("Execution failed.");
	}
	else
	{
		ptrace(PTRACE_SEIZE, pid, 0, PTRACE_O_TRACESYSGOOD);
		sigprocmask(SIG_SETMASK, &empty, NULL);
		waitpid(pid, 0, 0);
		sigprocmask(SIG_BLOCK, &blocked, NULL);
		trace_loop(pid);
	}
	return (1);
}
