/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_path.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/28 16:37:41 by tlovato           #+#    #+#             */
/*   Updated: 2019/02/28 16:37:42 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_strace.h"

static int		calc_len(char *path)
{
	int			i = 0;
	int			ret = 0;

	while (path[i])
	{
		if (path[i] == ':')
			ret ++;
		i ++;
	}
	return (ret + 1);
}

char			**split_path(char *path, char c)
{
	int			len = calc_len(path);
	char		**ret = NULL;
	int			i_ret = 0;
	int			i_path = 0;
	int			save = 0;

	if (!(ret = (char **)malloc(sizeof(char *) * len + 1)))
		return (NULL);
	ret[len] = NULL;
	while (path[i_path])
	{
		if (path[i_path] == ':')
		{
			ret[i_ret] = strndup(&path[save], i_path - save);
			i_ret ++;
			save = i_path + 1;
		}
		else if (!path[i_path + 1])
			ret[i_ret] = strndup(&path[save], i_path + 1 - save);
		i_path ++;
	}
	return (ret);
}

char			**get_path(char **env)
{
	int			i = 0;
	char		*path;
	char		**ret = NULL;

	while (env[i])
	{
		if (!(strncmp(env[i], "PATH=", 5)))
			path = strdup(&env[i][5]);
		i++;
	}
	ret = split_path(path, ':');
	return (ret);
}

