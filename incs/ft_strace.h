/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strace.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/28 15:17:31 by tlovato           #+#    #+#             */
/*   Updated: 2019/02/28 15:17:32 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STRACE_H
# define FT_STRACE_H

# include	"../libft/libft.h"
# include 	<stdio.h>
# include	<stdint.h>
# include 	<unistd.h>
# include 	<sys/types.h>
# include 	<sys/wait.h>
# include	<string.h>
# include	<stdlib.h>
# include	<dirent.h>
# include	<sys/ptrace.h>
# include	<sys/user.h>
# include	<signal.h>

struct user_regs_struct_32 {
	uint32_t ebx;
	uint32_t ecx;
	uint32_t edx;
	uint32_t esi;
	uint32_t edi;
	uint32_t ebp;
	uint32_t eax;
	uint32_t xds;
	uint32_t xes;
	uint32_t xfs;
	uint32_t xgs;
	uint32_t orig_eax;
	uint32_t eip;
	uint32_t xcs;
	uint32_t eflags;
	uint32_t esp;
	uint32_t xss;
};

int		error_handling(char *message);
char		**get_path(char **env);
char		*find_command(char *cmd, char **path);
int		trace(char *path, char **args, char **env);
void		print_infos(struct user_regs_struct regs);
void		print_infos32(struct user_regs_struct_32 regs);
void		print_return(struct user_regs_struct regs);
void		print_return32(struct user_regs_struct_32 regs);

#endif
