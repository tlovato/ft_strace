# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tlovato <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/02/28 15:17:39 by tlovato           #+#    #+#              #
#    Updated: 2019/02/28 15:17:40 by tlovato          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_strace

SRCS_DIR = srcs/
SRCS_FILES = ft_strace.c \
	get_path.c \
	find_command.c \
	trace.c \
	print_infos.c

SRCS = $(addprefix $(SRCS_DIR), $(SRCS_FILES))
OBJS = $(SRCS:.c=.o)

all : $(NAME)

%.o : %.c
	gcc -g3 -c $< -o $@

$(NAME) : $(OBJS)
	make -C libft/
	gcc -g3 -o $@ $^ -L libft/ -lft 

clean : 
	rm -f $(OBJS)
	cd libft/ && make clean

fclean : clean
	rm -rf $(NAME)
	cd libft/ && make fclean

re : fclean all

.PHONY : all clean fclean re
