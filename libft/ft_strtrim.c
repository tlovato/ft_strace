/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 15:24:17 by tlovato           #+#    #+#             */
/*   Updated: 2015/12/08 11:09:32 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strtrim(const char *s)
{
	char	*dup;
	int		i;
	int		j;
	int		k;

	i = 0;
	j = ft_strlen(s) - 1;
	k = 0;
	while (s[i] == ' ' || s[i] == '\t' || s[i] == '\n')
		i++;
	while ((s[j] == ' ' || s[j] == '\t' || s[j] == '\n') && j != 0)
		j--;
	if (j < i)
		j = ft_strlen(s);
	if ((dup = (char *)malloc(sizeof(char) * ((j - i) + 2))) == 0)
		return (NULL);
	while (i <= j)
	{
		dup[k] = s[i];
		k++;
		i++;
	}
	dup[k] = '\0';
	return (dup);
}
