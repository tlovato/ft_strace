/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 09:56:09 by tlovato           #+#    #+#             */
/*   Updated: 2015/12/06 14:07:35 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strcmp(const char *str, const char *str2)
{
	int		i;

	i = 0;
	while ((str[i] && str2[i]) && (str[i] == str2[i]))
		i++;
	return ((unsigned char)str[i] - (unsigned char)str2[i]);
}
